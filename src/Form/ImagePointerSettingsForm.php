<?php

namespace Drupal\image_pointer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\Entity\File;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

/**
 * Configurarion form for Image Pointer.
 */
class ImagePointerSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'image_pointer.settings';

  /**
   * Theme EntityType Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The config service.   
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(     
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'image_pointer_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach($types as $key => $value) {
      $content_type[$key] = $value->get('name');      
    }
    $config = $this->config(static::SETTINGS)->get();
    $form['content_type'] = [
      '#type' => 'select',
      '#weight' => '1',
      //'#multiple' => TRUE,
      '#options' => $content_type,
      '#title' => $this->t('Content Type'),
      '#default_value' => isset($config['content_type']) ? $config['content_type'] : [],
      '#required' => TRUE,
      '#description' => $this->t('Please choose the Content Type to create a map pointer
                                (Two fields will be created for the selected content type).'),
      
    ];
    if(!empty($config['content_type'])) {
      $form['content_type']['#disabled'] = TRUE;
      $type = "/admin/config/image-pointer/" . $config['content_type'] . "/delete";
      $form['content_type']['#description'] = $this->t('<a href="'. $type . '">Update Content type</a>');
    }
    $form['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Static Image.'),
      '#upload_validators' => array(
          'file_validate_extensions' => array('gif png jpg jpeg'),
          //'file_validate_size' => array(256000000),
      ),
      '#theme' => 'image_widget',
      '#preview_imgage_style' => 'medium',
      '#upload_location' => 'public://image_pointer',
      '#progress_message' => $this->t('It is in progress...'),
      '#default_value' => isset($config['image']) ? $config['image'] : '',
      '#required' => TRUE,
    ];
    $form['image_pointer'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image Pointer.'),
      '#upload_validators' => array(
          'file_validate_extensions' => array('gif png jpg jpeg'),
         // 'file_validate_size' => array(25600),
      ),
      '#theme' => 'image_widget',
      '#preview_imgage_style' => 'medium',
      '#upload_location' => 'public://image_pointer',
      '#progress_message' => $this->t('It is in progress...'),
      '#default_value' => isset($config['image_pointer']) ? $config['image_pointer'] : '',
      '#required' => TRUE,
    ];
    $form['block_title'] = [
      '#type' => 'textfield',
      '#weight' => '2',
      '#title' => $this->t('Block Title'),
      '#default_value' => isset($config['block_title']) ? $config['block_title'] : '',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#description' => $this->t('Please fill the text for Block title.'),
    ];
    $form['image_width'] = [
      '#type' => 'textfield',
      '#weight' => '2',
      '#title' => $this->t('Image Width'),
      '#default_value' => isset($config['image_width']) ? $config['image_width'] : '',
      '#maxlength' => 10,
      '#required' => TRUE,
      '#description' => $this->t('Please fill the width of the uploaded image.(Example: 400).'),
    ];
    $form['image_height'] = [
      '#type' => 'textfield',
      '#weight' => '2',
      '#title' => $this->t('Image Height'),
      '#default_value' => isset($config['image_height']) ? $config['image_height'] : '',
      '#maxlength' => 10,
      '#required' => TRUE,
      '#description' => $this->t('Please fill the height of the uploaded image.(Example: 400).'),
    ];
    $form['marker_width'] = [
      '#type' => 'textfield',
      '#weight' => '2',
      '#title' => $this->t('Marker Width'),
      '#default_value' => isset($config['marker_width']) ? $config['marker_width'] : '',
      '#maxlength' => 10,
      '#required' => TRUE,
      '#description' => $this->t('Please fill the width of the uploaded marker.(Example: 50).'),
    ];
    $form['marker_height'] = [
      '#type' => 'textfield',
      '#weight' => '2',
      '#title' => $this->t('Marker Height'),
      '#default_value' => isset($config['marker_height']) ? $config['marker_height'] : '',
      '#maxlength' => 10,
      '#required' => TRUE,
      '#description' => $this->t('Please fill the height of the uploaded marker.(Example: 50).'),
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
   
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.

    $config = $this->config(static::SETTINGS);
    $content_type = $form_state->getValue('content_type');    
    $image = $form_state->getValue('image');
    $image_pointer = $form_state->getValue('image_pointer');
    $image_width = $form_state->getValue('image_width');
    $image_height = $form_state->getValue('image_height');
    $marker_width = $form_state->getValue('marker_width');
    $marker_height = $form_state->getValue('marker_height');
    $block_title = $form_state->getValue('block_title');
    
    if ($image != $this->config['image']) {
      if (!empty($image[0])) {
          $file = File::load($image[0]);
          $file->setPermanent();
          $file->save;
          \Drupal::service('file.usage')->add($file, 'image_pointer', 'file', $file->id());
      }
    }
    if ($image_pointer != $this->config['image_pointer']) {
      if (!empty($image_pointer[0])) {
          $file = File::load($image_pointer[0]);
          $file->setPermanent();
          $file->save;
          \Drupal::service('file.usage')->add($file, 'image_pointer', 'file', $file->id());
      }
    }
    $config->set('content_type', $content_type);
    $config->set('image', $image);
    $config->set('image_pointer', $image_pointer);
    $config->set('image_width', $image_width);
    $config->set('image_height', $image_height);
    $config->set('marker_width', $marker_width);
    $config->set('marker_height', $marker_height);
    $config->set('block_title', $block_title);   
      

    // Create Image pointer field.
    $field_name  = 'field_image_pointer_xy';
    $bundle = $content_type;
    $entity_type = 'node';
    $type = 'text';
    $label = 'Pointer (x,y)';
    $widget = 'text_textfield';
    $field_storage = FieldStorageConfig::loadByName($entity_type, $field_name);
    if (empty($field_storage)) {
      $this->createFieldType($field_name, $bundle, $entity_type, $type, $label, $widget);
    }

    // Create pointer description filed
    $field_name  = 'field_image_pointer_desc';
    $bundle = $content_type;
    $entity_type = 'node';
    $type = 'text_with_summary';
    $label = 'Marker Description';
    $widget = 'text_textarea_with_summary';
    $field_storage = FieldStorageConfig::loadByName($entity_type, $field_name);
    if (empty($field_storage)) {
      $this->createFieldType($field_name, $bundle, $entity_type, $type, $label, $widget);
    }


    // Deleting field storage.
//FieldStorageConfig::loadByName('entity_type', 'field_name')->delete();

// Deleting field.
//FieldConfig::loadByName('entity_type', 'bundle', 'field_name')->delete();
    
    
    


    /*
    $entity_type = $form_state->getValue('entity_type');
    $url = $form_state->getValue('url');
    $pages = $form_state->getValue('pages');
    $this->config(static::SETTINGS)->delete();
    $config = $this->config(static::SETTINGS);
    foreach ($entity_type as $key => $value) {
      $config->set('entity_type.' . $key, $value);
    }
    $config->set('url', $url);
    $config->set('pages', $pages);
    */
    $config->save();
    parent::submitForm($form, $form_state);
  }

  public function createFieldType($field_name, $bundle, $entity_type, $type, $label) {
    $field_storage = FieldStorageConfig::loadByName($entity_type, $field_name);
    if (empty($field_storage)) {
      $field_instance = FieldStorageConfig::create(array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'type' => $type,
      ))->save();
      $config_array = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'label' => $label,        
      );
      
      $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);
      if (empty($field) && $bundle !== "" && !empty($bundle)) {
        FieldConfig::create($config_array)->save();
      }
      entity_get_form_display($entity_type, $bundle, 'default')->setComponent($field_name, array(
        'type' => $widget,
      ))->save(); 
    }

  }

}
