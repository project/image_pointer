<?php

namespace Drupal\image_pointer\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

/**
 * Defines a confirmation form to update content type.
 */
class UpdateContentTypeForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var string
   */
  protected $type;

   /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'image_pointer.settings';

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config service.  
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config;   
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),      
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $type = NULL) {
    $this->type = $type;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {    
    $config = $this->config->getEditable(static::SETTINGS);
    $content_type = $config->get('content_type');
    if(!empty($config->get('content_type'))) {
      $entity_type = 'node';
      $field_name = 'field_image_pointer_xy';
      $field_storage = $this->getFieldStorage($entity_type, $field_name);
      if (!empty($field_storage)) {
        $this->deleteFieldConfig($entity_type, $field_name, $content_type);
      }
      $entity_type = 'node';
      $field_name = 'field_image_pointer_desc';
      $field_storage = $this->getFieldStorage($entity_type, $field_name);
      if (!empty($field_storage)) {
        $this->deleteFieldConfig($entity_type, $field_name, $content_type);
      }
      $config->set('content_type', '');
      $config->save();
    } 
    $form_state->setRedirect('image_pointer.config.form');
    return;   
  }

  /**
   * Get field storage.
   */
  public function getFieldStorage($entity_type, $field_name) {
    return FieldStorageConfig::loadByName($entity_type, $field_name);
  }

  /**
   * Delete field storage and field.
   */
  public function deleteFieldConfig($entity_type, $field_name, $content_type) {    
    FieldConfig::loadByName($entity_type, $content_type, $field_name)->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "update_content_type_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('image_pointer.config.form');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('<p>Updating content type will delete Image Pointer settings.Do you want to proceed?.</p>');
  }

}