<?php

namespace Drupal\image_pointer\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\Entity\File;

/**
 * Image Pointer View.
 *
 * @Block(
 *   id = "image_pointer_view_block",
 *   admin_label = @Translation("Image Pointer View Block"),
 *   category = @Translation("Image Pointer View Block"),
 * )
 */
class ImagePointerViewBlock extends BlockBase implements ContainerFactoryPluginInterface {
  
  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config service.  
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct(array $configuration, 
                              $plugin_id, 
                              $plugin_definition,
                              ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager
                              ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager')   
    );
  }

 
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->config->get('image_pointer.settings')->get();
    
    if(!empty($config)) {
      $properties = [
        'type' => $config['content_type'],
        'status' => 1,    
      ];      
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties($properties);
      $file = File::load($config['image'][0]);  
      $file_marker = File::load($config['image_pointer'][0]);
      if(!empty($file) && !empty($file_marker)) {
        $url = $file->url(); 
        $marker_url = $file_marker->url();
        $arg['image_url'] = $url;
        $arg['marker_url'] = $marker_url;
      }      
      $arg['image_width'] = $config['image_width'];
      $arg['image_height'] = $config['image_height'];
      $arg['marker_width'] = $config['marker_width'];
      $arg['marker_height'] = $config['marker_height'];
      foreach($nodes as $key => $node) {        
        $pointer = $node->get('field_image_pointer_xy')->getValue()[0]['value'];
        if(!empty($pointer)) {
          $arr_coord = explode(',', $pointer);
          $contents[$key]['pointerX'] = $arr_coord[0];
          $contents[$key]['pointerY'] = $arr_coord[1];
          $contents[$key]['description'] = $node->get('field_image_pointer_desc')->getValue()[0]['value']; 
          $contents[$key]['title'] = $node->get('title')->getValue()[0]['value'];  
        }            
      }
    }
    //print_r($content);
    return [
      '#theme' => 'image_pointer_view',
      '#contents' => $contents,
      '#arg' => $arg,
      '#attached' => [
        'library' => [
          'image_pointer/image_pointer.view.lib',
        ],
      ],  
    ];
  }

}