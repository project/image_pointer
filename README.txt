INTRODUCTION
------------

Image Pointer is a simple module for creating a marker on a static image.
It allows site admin to upload their own image to create a marker on that image.
The list of marker with the static image will be displayed as a block.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install the Server IP module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.    

CONFIGURATION
-------------

* Configure the Image Pointer Settings in 
  Administration » Configuration » Image Pointer:

1. After install this module, we can see the menu 'Image Pointer' in the
   admin configuration menu. 
2. By default, administrator role can access this url. If we want to access
   this url to different role, go to permission page and find the word
   'Edit Image pointer Settings' to set the permission for different role.

  Usage: 

  * Once submitted the configuration, Image pointer fields will be available 
    in the selected content type.
  * In the create content section, place the marker on the image as per your requirements.
  * All the marker will be displayed as block.
  * Place the block "Image Pointer View Block" into region to see the marker list.

MAINTAINERS
-----------

Current maintainers:
  Elavarasan R - https://www.drupal.org/user/1902634

