/**
 * @file
 * It contains the basic function for creating Image pointer.
 */

(function($){ 
  $(document).ready(function() {
    var marker_width = $('.mappoint').attr('width');
    var marker_height = $('.mappoint').attr('width');    
    $("#dismap").click(function(event) {      
      var offsetX = $(this).offset().left;
      var offsetY = $(this).offset().top;      
      var pointX = event.pageX - offsetX;
      var pointY = event.pageY - offsetY;
      $('.mappoint').css('margin-left', pointX-(marker_width/2) + "px");
      $('.mappoint').css('margin-top', pointY-marker_height + "px");
      var xy = (pointX-(marker_width/2)).toString() + "," + (pointY-marker_height).toString();      
      $('#edit-field-image-pointer-xy-0-value').val(xy);
    });
  });
})(jQuery); 
