/**
 * @file
 * It contains the basic function for creating Image pointer.
 */

(function($){ 
  $(document).ready(function() {
    $(".mappoint").click(function(event) { 
      $('.pointer-tooltip-class').hide();   
      var key = $(this).attr('pointer-tooltip');  
      $("#pointer-tooltip" + key).show();   
      event.stopPropagation();   
    });
    $(document).click(function() {
      $('.pointer-tooltip-class').hide(); 
    });
  });  
})(jQuery); 
